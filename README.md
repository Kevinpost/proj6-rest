# README #
# proj6-rest
## **Author Information**

* Name:  Created by R Durairajan; uploaded to Bitbucket by Kevin Post

* Contact address: Kpost7@uoregon.edu


I have been occupied by extra-curricular events that demand my immediate attention, and did not do this project.

I am counting on 35 points for the following:

*  you'll get 35 points assuming:

    * README is updated with your name and email ID.  (done)
    * The credentials.ini is submitted with the correct URL of your repo. (done)
    * Dockerfile is present. (done)
    * Docker-compose.yml works/builds without any errors. (done)
